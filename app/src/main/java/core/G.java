package core;

import android.app.Activity;
import android.app.Application;
import android.app.NativeActivity;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;

import java.util.ArrayList;

import apiservice.ApiService;
import app.struct.App;
import ir.vahidgarousi.app.androidmarket.R;
import main.activity.MainActivity;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by developer on 3/23/2018
 */

public class G extends Application {

    public static Context context;
    public static LayoutInflater layoutInflater;
    public static AppCompatActivity activity;
    public static Activity currentActivity;
    public static Typeface typefaceIranSansLight;
    public static Typeface typefaceIranSansBold;
    private static final String TAG = "G";
    public static ApiService apiService;
    public static App app;
    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        layoutInflater = LayoutInflater.from(context);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("font/iran_sans_light.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        typefaceIranSansLight = Typeface.createFromAsset(getAssets(), "font/iran_sans_light.ttf");
        typefaceIranSansBold = Typeface.createFromAsset(getAssets(), "font/iran_sans_bold.ttf");
        apiService = new ApiService(context);
    }

    public static ArrayList<App> generateApps() {

        ArrayList<App> appArrayList = new ArrayList<>();
        for (int i = 0; i <= 10; i++) {
            App app = new App();
            app.setName("برنامه " + i);
            app.setPrice("رایگان " + i);
            app.setLogoUrl("https://s.cafebazaar.ir/1/icons/com.PaeezanStudio.NanJoon_128x128.png");
            appArrayList.add(app);
        }
        return appArrayList;
    }
}
