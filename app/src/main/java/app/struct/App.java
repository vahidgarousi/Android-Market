
package app.struct;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import util.NumberConverter;

@SuppressWarnings("unused")
public class App implements Parcelable {


    public static final int DEFAULT_SORT = 1;
    @SerializedName("author")
    private String Author;
    @SerializedName("description")
    private String Description;
    @SerializedName("email")
    private String Email;
    @SerializedName("fileSize")
    private String FileSize;
    @SerializedName("id")
    private String Id;
    @SerializedName("installs")
    private String Installs;
    @SerializedName("logoUrl")
    private String LogoUrl;
    @SerializedName("name")
    private String Name;
    @SerializedName("phoneNumber")
    private String PhoneNumber;
    @SerializedName("price")
    private String Price;
    @SerializedName("ratingValue")
    private String RatingValue;
    @SerializedName("versionName")
    private String VersionName;
    @SerializedName("votes")
    private String Votes;

    public String getAuthor() {
        return NumberConverter.StringRequestConvertToFarsiNumbers(Author);
    }

    public void setAuthor(String author) {
        Author = author;
    }

    public String getDescription() {
        return NumberConverter.StringRequestConvertToFarsiNumbers(Description);
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getFileSize() {
        return NumberConverter.StringRequestConvertToFarsiNumbers(FileSize);
    }

    public void setFileSize(String fileSize) {
        FileSize = fileSize;
    }

    public String getId() {
        return NumberConverter.StringRequestConvertToFarsiNumbers(Id);
    }

    public void setId(String id) {
        Id = id;
    }

    public String getInstalls() {
        return NumberConverter.StringRequestConvertToFarsiNumbers(Installs);
    }

    public void setInstalls(String installs) {
        Installs = installs;
    }

    public String getLogoUrl() {
        return LogoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        LogoUrl = logoUrl;
    }

    public String getName() {
        return NumberConverter.StringRequestConvertToFarsiNumbers(Name);
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPhoneNumber() {
        return NumberConverter.StringRequestConvertToFarsiNumbers(PhoneNumber);
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    public String getPrice() {
        return NumberConverter.StringRequestConvertToFarsiNumbers(Price);
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getRatingValue() {
        return NumberConverter.StringRequestConvertToFarsiNumbers(RatingValue);
    }

    public void setRatingValue(String ratingValue) {
        RatingValue = ratingValue;
    }

    public String getVersionName() {
        return NumberConverter.StringRequestConvertToFarsiNumbers(VersionName);
    }

    public void setVersionName(String versionName) {
        VersionName = versionName;
    }

    public String getVotes() {
        return NumberConverter.StringRequestConvertToFarsiNumbers(Votes);
    }

    public void setVotes(String votes) {
        Votes = votes;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.Author);
        dest.writeString(this.Description);
        dest.writeString(this.Email);
        dest.writeString(this.FileSize);
        dest.writeString(this.Id);
        dest.writeString(this.Installs);
        dest.writeString(this.LogoUrl);
        dest.writeString(this.Name);
        dest.writeString(this.PhoneNumber);
        dest.writeString(this.Price);
        dest.writeString(this.RatingValue);
        dest.writeString(this.VersionName);
        dest.writeString(this.Votes);
    }

    public App() {
    }

    protected App(Parcel in) {
        this.Author = in.readString();
        this.Description = in.readString();
        this.Email = in.readString();
        this.FileSize = in.readString();
        this.Id = in.readString();
        this.Installs = in.readString();
        this.LogoUrl = in.readString();
        this.Name = in.readString();
        this.PhoneNumber = in.readString();
        this.Price = in.readString();
        this.RatingValue = in.readString();
        this.VersionName = in.readString();
        this.Votes = in.readString();
    }

    public static final Parcelable.Creator<App> CREATOR = new Parcelable.Creator<App>() {
        @Override
        public App createFromParcel(Parcel source) {
            return new App(source);
        }

        @Override
        public App[] newArray(int size) {
            return new App[size];
        }
    };
}
