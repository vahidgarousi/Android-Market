//package NavigationCategory.adapter;
//
//import android.net.Uri;
//import android.support.annotation.IdRes;
//import android.support.v7.widget.RecyclerView;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.squareup.picasso.Picasso;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import app.struct.App;
//import core.G;
//import ir.vahidgarousi.app.androidmarket.R;
//
//
///**
// * Created by developer on 3/26/2018
// */
//
//public class BestFragmentApplicationsAdapter extends RecyclerView.Adapter<BestFragmentApplicationsAdapter.MyViewHolder> {
//
//    private static final String TAG = "BestFragmentApplication";
//    private List<App> apps = new ArrayList<>();
//    private static MyViewHolder.onAppClick onAppClick;
//
//    private class VIEWS_TYPES {
//        private static final int HEADER = 1;
//        private static final int ITEM = 2;
//    }
//
//    private boolean isHeader = false;
//    private boolean isItem = false;
//
//    public BestFragmentApplicationsAdapter(List<App> apps, MyViewHolder.onAppClick onAppClick) {
//        this.apps = apps;
//        this.onAppClick = onAppClick;
//    }
//
//
//    @Override
//    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View rootView;
//        switch (viewType) {
//            case VIEWS_TYPES.HEADER:
//                isHeader = true;
//                isItem = false;
//                rootView = G.layoutInflater.inflate(R.layout.item_best_fragment_banner, parent, false);
//                break;
//            case VIEWS_TYPES.ITEM:
//                isHeader = false;
//                isItem = true;
//                rootView = G.layoutInflater.inflate(R.layout.item_best_app, parent, false);
//                break;
//            default:
//                isHeader = true;
//                isItem = false;
//                rootView = G.layoutInflater.inflate(R.layout.item_best_fragment_banner, parent, false);
//        }
//        return new MyViewHolder(rootView);
//    }
//
//
//    @Override
//    public void onBindViewHolder(MyViewHolder holder, int position) {
//        if (isHeader) {
//            holder.bindBanner(apps.get(position).getImage());
//        } else if (isItem) {
//            holder.bindApplications(apps.get(position));
//        }
//    }
//
//    @Override
//    public int getItemViewType(int position) {
//        if (apps.get(position).isHeader()) {
//            return VIEWS_TYPES.HEADER;
//        } else {
//            return VIEWS_TYPES.ITEM;
//        }
//    }
//
//    @Override
//    public int getItemCount() {
//        return apps.size();
//    }
//
//    public void addApplication(ArrayList<App> applications) {
//        apps.addAll(applications);
//        notifyDataSetChanged();
//    }
//
//    public static class MyViewHolder extends RecyclerView.ViewHolder {
//        private ImageView bannerImageView;
//        private ImageView appImageImageView;
//        private TextView appNameTextView;
//
//        public MyViewHolder(View itemView) {
//            super(itemView);
//            bannerImageView = itemView.findViewById(R.id.iv_itemBestFragment_banner);
//            appImageImageView = itemView.findViewById(R.id.iv_itemBestApp_appImage);
//            appNameTextView = itemView.findViewById(R.id.tv_itemBestApp_appName);
//        }
//
//        public void bindBanner(String image) {
//            Picasso.get().load(Uri.parse(image)).into(bannerImageView);
//        }
//
//        public void bindApplications(final App app) {
//            Log.d(TAG, "bindApplications() called with: app = [" + app.getName() + "]");
//            Log.d(TAG, "bindApplications() called with: app = [" + app.getImage() + "]");
//            Log.d(TAG, "bindApplications() called with: app = [" + app.isHeader() + "]");
//            appNameTextView.setText(app.getName());
//            Picasso.get().load(Uri.parse(app.getImage())).into(appImageImageView);
//            itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    onAppClick.onAppClicked(app);
//                }
//            });
//        }
//
//        public interface onAppClick {
//            void onAppClicked(App app);
//        }
//    }
//
//
//}
