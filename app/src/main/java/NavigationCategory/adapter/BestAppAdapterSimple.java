package NavigationCategory.adapter;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import app.struct.App;
import core.G;
import ir.vahidgarousi.app.androidmarket.R;

/**
 * Created by developer on 3/27/2018
 */

public class BestAppAdapterSimple extends RecyclerView.Adapter<BestAppAdapterSimple.BestAppViewHolder> {

    private List<App> apps = new ArrayList<>();
    private static BestAppViewHolder.onAppClick onAppClick;
    public BestAppAdapterSimple(ArrayList<App> apps,BestAppViewHolder.onAppClick onAppClick) {
        this.apps = apps;
        this.onAppClick = onAppClick;
    }

    @Override
    public BestAppViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = G.layoutInflater.inflate(R.layout.item_best_app,parent,false);
        return new BestAppViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(BestAppViewHolder holder, int position) {
        holder.bindApplications(apps.get(position));
    }

    @Override
    public int getItemCount() {
        return apps.size();
    }

    public void addApplication(ArrayList<App> applications) {
        apps.addAll(applications);
        notifyDataSetChanged();
    }

    public static class BestAppViewHolder extends RecyclerView.ViewHolder {
        private ImageView bannerImageView;
        private ImageView appImageImageView;
        private TextView appNameTextView;
        private TextView appVoteTextView;
        private Button appRunButton;

        public BestAppViewHolder(View itemView) {
            super(itemView);
            //bannerImageView = itemView.findViewById(R.id.iv_itemBestFragment_banner);
            appImageImageView = itemView.findViewById(R.id.iv_itemBestApp_appImage);
            appNameTextView = itemView.findViewById(R.id.tv_itemBestApp_appName);
            appVoteTextView = itemView.findViewById(R.id.tv_itemBestApp_vote);
            appRunButton = itemView.findViewById(R.id.tv_itemBestApp_runApp);
            appNameTextView.setTypeface(G.typefaceIranSansLight);
            appVoteTextView.setTypeface(G.typefaceIranSansLight);
            appRunButton.setTypeface(G.typefaceIranSansLight);
        }
        public void bindApplications(final App app) {
            if (!app.getName().isEmpty() && !app.getLogoUrl().isEmpty()) {
                appNameTextView.setText(app.getName() + "");
                Picasso.get().load(Uri.parse(app.getLogoUrl())).into(appImageImageView);
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onAppClick.onAppClicked(app);
                    }
                });
            }
        }
        public interface onAppClick {
            void onAppClicked(App app);
        }
    }
}
