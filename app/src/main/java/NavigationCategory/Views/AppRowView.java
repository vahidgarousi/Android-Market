package NavigationCategory.Views;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import app.adapter.ApplicationAdapter;
import app.struct.App;
import core.G;
import ir.vahidgarousi.app.androidmarket.R;

/**
 * Created by developer on 3/24/2018
 */

public class AppRowView extends RelativeLayout {

    private TextView appCategoryTitleTextView;
    private TextView viewAppList_allAppTextView;
    private LinearLayout viewAllAppLinearLayout;
    private RecyclerView appRecyclerView;

    public AppRowView(Context context) {
        super(context);
        init();
    }

    public AppRowView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AppRowView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();

    }

    public AppRowView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();

    }

    private void init() {
        View view = G.layoutInflater.inflate(R.layout.item_main_fragment, this, true);
        appCategoryTitleTextView = view.findViewById(R.id.tv_viewAppList_title);
        viewAppList_allAppTextView = view.findViewById(R.id.tv_viewAppList_allApp);
        viewAllAppLinearLayout = view.findViewById(R.id.ll_viewAppList_allApp);
        appRecyclerView = view.findViewById(R.id.rl_viewAppList_recyclerView);
        appCategoryTitleTextView.setTypeface(G.typefaceIranSansBold);
        viewAppList_allAppTextView.setTypeface(G.typefaceIranSansBold);
    }

    public void attachApplications(ArrayList<App> applications, ApplicationAdapter.ApplicationViewHolder.onApplicationClick onApplicationClick) {
        appRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        ApplicationAdapter adapter = new ApplicationAdapter(onApplicationClick, R.layout.item_application);
        adapter.addApplication(applications);
        appRecyclerView.setAdapter(adapter);
    }

    public void setOnShowAllButtonClickListener(OnClickListener onClickListener) {

    }

    public void setRowTitleTextView(String titleTextView) {

    }


}
