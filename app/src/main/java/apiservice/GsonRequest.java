package apiservice;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;

/**
 * Created by vahid on 3/18/2018
 */

public class GsonRequest<T> extends Request<T> {

    private Gson gson = new Gson();
    private static final String TAG = "GsonRequest";
    private Response.Listener<T> responseListener;
    private String requestBody;
    private Type type;
    private Context context;

    public GsonRequest(Context context, int method, String url, String requestBody, Response.Listener<T> responseListener, Response.ErrorListener listener, Type type) {
        super(method, url, listener);
        this.responseListener = responseListener;
        this.requestBody = requestBody;
        this.type = type;
        this.context = context;
        setRetryPolicy(new DefaultRetryPolicy(16000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    public GsonRequest(int method, String url, String requestBody, Response.Listener<T> responseListener, Response.ErrorListener listener, Type type) {
        super(method, url, listener);
        this.responseListener = responseListener;
        this.requestBody = requestBody;
        this.type = type;
        setRetryPolicy(new DefaultRetryPolicy(16000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String responseString = new String(response.data, "UTF-8");
            T result = gson.fromJson(responseString, type);
            return Response.success(result, null);

        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, "parseNetworkResponse: " + e.toString());
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected void deliverResponse(T response) {
        responseListener.onResponse(response);
        Log.d(TAG, "deliverResponse() called with: response = [" + response + "]");
    }

}
